package Logic;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import GUI.GUI;


/**
 * ������ ���� ���������� ��������� �� ������� ������ � ������ GUI
 * ������������ ����������� ����� - ActionListener
 * @author Alexander Sherbakov
 *
 */
public class MyActionListener implements ActionListener {

    // ��������� ����� GUI ��� ���� ����� �������� ������ � �������.
    private GUI myGUI;
    private DataSet myData;
    /**
     * ����������� ������ 
     * @param gui ��������� ������ � GUI, ��� �������� ����� ����������� ��������� �������
     * @param data ��������� ������ � �������
     */
    public MyActionListener(GUI gui, DataSet data) 
    {
        myGUI = gui;
        myData = data;
    }

    /**
     * ����� ���������� �������, ���������� ����� actionPerformed. ����� ���������� ��� ���������.
     */
    public void actionPerformed(ActionEvent e) 
    {
    	if(e.getSource() instanceof JButton) 
    	{
    		Object property = ((JButton)(e.getSource())).getClientProperty("action");
    		if (property instanceof String)
    		{
    			//New game
    			String act = (String)property;
    			if(act.equals("new_game"))
    			{
    				myGUI.clearButtons();
    				myGUI.enableButtons(true);
    				myData.init();
    				myGUI.setStatus("����� ����� "+ myData.getPlayerTurnNum());	
    			}
    		}
    		
    		//���� ��������� ������ � ����?
    		int x = -1, y = -1;
	    	property = ((JButton)(e.getSource())).getClientProperty("x");
	    	if (property instanceof Integer) {
	    	   x = ((Integer)property);
	    	}
	    	
	    	property = ((JButton)(e.getSource())).getClientProperty("y");
	    	if (property instanceof Integer) {
	    	   y = ((Integer)property);
	    	   
	    	}
	    	
	    	if(x>=0 && y>=0)
	    	{
	    		//System.out.println("X:" + x + "Y:" + y);
	    		myData.setFieldElement(x, y, myData.getPlayerTurnNum());
	    		boolean succsess = myData.solutionCheck();
	    		if(!succsess) myData.switchPlayer(); //������ �� ������ ��� �������� ������� ��� �� ����������
	    		myData.updateFreeDotsCount();
	    		myGUI.updateButtons();
	    		myGUI.setStatus("����� ����� "+ myData.getPlayerTurnNum());
	    	}
	    	
	    	if(myData.getFreeDotsCount() <= 0)
	    		myGUI.declareResults();
	    	
	    	
    	}
    }
}