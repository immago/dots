package Logic;
/**
 * � ������ ������ �������� ������ ���������
 * @author Alexander Sherbakov
 *
 */
public class DataSet 
{
	/**
	 * ������ ����
	 */
	final public static int WIDTH = 20; //x
	/**
	 * ������ ����
	 */
	final public static int HEIGHT = 20; //y
	
	private int scoreP1 = 0;
	private int scoreP2 = 0;
	private int tempScore = 0;
	private boolean isPlayerOneTurn = false;
	private int freeDotsCount;
	private boolean isPlayerCapchedEnemyDot = false;
	/**
	 * ������� ����
	 * 0 - ��������
	 * 1 - player 1
	 * 2 - palyer 2
	 */
	private int[][] field = new int[WIDTH][HEIGHT];
	
	/**
	 * ��������� ����� ������
	 * @param playerNum ����� ������
	 * @return ���� ������ ��� -1 ��� ������������ ������� ��������
	 */
	public int getPlayerScore(int playerNum)
	{
		if(playerNum == 1)
			return scoreP1;
		else if(playerNum == 2)
			return scoreP2;
		else
		{
			System.err.println("Wrong player");
			return -1;
		}
	}
	/**
	 * ��������� ����� ������
	 * @param playerNum ����� ������
	 * @param score ����
	 */
	public void setPlayerScore(int playerNum, int score)
	{
		if(playerNum == 1)
			scoreP1 = score;
		else if(playerNum == 2)
			scoreP2 = score;
		else
			System.err.println("Wrong player");
	
	}
	/**
	 * �������� �������� ����� ����� �����
	 * @return 1 ��� 2
	 */
	public int getPlayerTurnNum()
	{
		if(isPlayerOneTurn)
			return 1;
		else
			return 2;
	}
	/**
	 * ������� ������ (��������� ���)
	 */
	public void switchPlayer()
	{
		isPlayerOneTurn = !isPlayerOneTurn;
	}
	
	/**
	 * �������� ������ � ������� �����
	 * @return ������ � ������� �����
	 */
	public int[][] getFieldArray()
	{
		return field;
	}
	
	/**
	 * ���������� �������� �������� � ������� �������� ����
	 * @param x ���������� �
	 * @param y ���������� Y
	 * @param value ��������
	 */
	public void setFieldElement(int x, int y, int value)
	{
		field[x][y] = value;
	}
	
	/**
	 * �������� ���������� � ���-�� ��������� ����� (������)
	 */
	public void updateFreeDotsCount()
	{
		int dotsTmp = 0;
		for(int x=0; x<WIDTH; x++)
			for(int y=0; y<HEIGHT; y++)
					if(field[x][y] == 0)
						dotsTmp++;
		
		freeDotsCount= dotsTmp;
	}
	/**
	 * �������� ���������� ��������� �����
	 * @return ���-�� �����
	 */
	public int getFreeDotsCount()
	{
		return freeDotsCount;
	}
	
	/**
	 * �������������
	 * �������� � ������ ����� ����
	 */
	public void init()
	{
		isPlayerCapchedEnemyDot = false;
		freeDotsCount = WIDTH * HEIGHT;
		isPlayerOneTurn = true;
		scoreP1 = 0;
		scoreP2 = 0;
		for(int x=0; x<WIDTH; x++)
			for(int y=0; y<HEIGHT; y++)
				field[x][y] = 0;
	}


	/**
	 * �������� �������
	 * �������� ��� ��������� ��������� �������� ����
	 * @return ���� �� ����� ��������� ��������� �����
	 */
	public boolean solutionCheck()
	{	
		isPlayerCapchedEnemyDot = false;
		/*��������� ��� ���� �������� ������ ���� �� ������ �����*/
		for (int tx=0; tx<WIDTH; tx++)
		{
			for (int ty=0; ty<HEIGHT; ty++)
			{
				if(field[tx][ty]!=0) 
					solution(tx, ty, field[tx][ty]);
			}
		} 
		return isPlayerCapchedEnemyDot;
	}
	/**
	 * �������� ������� ����� ����������� �������� ����������, ����������� �� ����������� �� ������� ���������� �� ������ ��������� �� ������� ����������
	 * @param x ���������� �
	 * @param y ���������� Y
	 * @param player ������ ������ 1 ��� 2
	 */
	private void solution(int x, int y, int player)
	{
		//����� �������
		int[][] backup = new int[WIDTH][HEIGHT];
		for (int tx=0; tx<WIDTH; tx++)
			for (int ty=0; ty<HEIGHT; ty++)
				backup[tx][ty]=field[tx][ty];

		//��������� ��� ���� ������ ����� (x,y +-1)	
		
	//x+1
		tempScore=0;
		if(findZone(x+1,y,player) ) //���������, ��������� ��� ����
		{	
			if(tempScore>0) isPlayerCapchedEnemyDot = true;
			
			if(player==1)
				scoreP1 = scoreP1+ tempScore;
			else 
				scoreP2 = scoreP2+ tempScore;
			return;
		}else
		{ //��� ������� ��������������� ���� �����
			for (int tx=0; tx<WIDTH; tx++)
				for (int ty=0; ty<HEIGHT; ty++)
					field[tx][ty]=backup[tx][ty];
		}
	//y+1
		tempScore=0;
		if(findZone(x,y+1,player) ) //���������, ��������� ��� ����
		{	
			if(tempScore>0) isPlayerCapchedEnemyDot = true;
			if(player==1)
				scoreP1 = scoreP1+tempScore;
			else 
				scoreP2 = scoreP2+tempScore;
			return;
		}else
		{ //��� ������� ��������������� ���� �����
			for (int tx=0; tx<WIDTH; tx++)
				for (int ty=0; ty<HEIGHT; ty++)
					field[tx][ty]=backup[tx][ty];
		}
	//x-1
		tempScore=0;
		if(findZone(x-1,y,player) ) //���������, ��������� ��� ����
		{	
			if(tempScore>0) isPlayerCapchedEnemyDot = true;
			if(player==1)
				scoreP1 = scoreP1+tempScore;
			else 
				scoreP2 = scoreP2+tempScore;
			return;
		}else
		{ //��� ������� ��������������� ���� �����
			for (int tx=0; tx<WIDTH; tx++)
				for (int ty=0; ty<HEIGHT; ty++)
					field[tx][ty]=backup[tx][ty];
		}
	//y-1
		tempScore=0;
		if(findZone(x,y-1,player) ) //���������, ��������� ��� ����
		{	
			if(tempScore>0) isPlayerCapchedEnemyDot = true;
			if(player==1)
				scoreP1 = scoreP1+tempScore;
			else 
				scoreP2 = scoreP2+tempScore;
			return;
		}else
		{ //��� ������� ��������������� ���� �����
			for (int tx=0; tx<WIDTH; tx++)
				for (int ty=0; ty<HEIGHT; ty++)
					field[tx][ty]=backup[tx][ty];
		}
		
	}
	/**
	 * ����������� �������� �������� ������ 
	 * @param x ���������� �� �
	 * @param y ���������� �� Y
	 * @param player ������ ������ 1 ��� 2
	 * @return true, ���� �������� � �����
	 */
	private boolean findZone(int x, int y, int player)
	{

	if ((x<0) || (x>WIDTH-1) || (y<0) || (y>HEIGHT-1)) return false; //������ �� ���� ���� - ������� �� ��������

	if (field[x][y]==player) return true; //��������� �� ���� ������ - ��� ��, �� ������ �� ����

	if(field[x][y]!=0) tempScore++; //���� ��������� ����� ������
	field[x][y]=player; //������ ��� ����, player-���� :)

	 //���� ����� ��� ���� �������. � ��� ��� ��, ��������� ������� �� ���
	return findZone(x-1,y,player)&&findZone(x+1,y,player)&&findZone(x,y+1,player)&&findZone(x,y-1,player);
	}

}
