package GUI;

import javax.swing.*;
import Logic.DataSet;
import Logic.MyActionListener;
import java.awt.*;

/**
 * ������ ����� ������� �� ����������� ������������ ����������
 * @author Alexander Sherbakov
 */
public class GUI 
{
    // ������� ����� - ����, ������� ����� ����������� ������������� � ��������� 
    private JFrame frame;
    private JPanel panelField, panelControls;
    // ������ ����� ����, ������ � �������� �������� ����
    private JButton newB, buttons[];
    
    private JLabel scoreP1Label, scoreP2Label, statusLabel;

    //����� ������������ ��������� ��� ������� ������
    private MyActionListener action;
    //����� � ������ �������
    private DataSet myData; 

   /**
    * ����������� ������, � ��� ���������� ������������� ���������� � ��������������� �������
    * @param data ��������� ������ � �������
    */
    public GUI(DataSet data) {

    	myData = data;
        frame = new JFrame("�����");

        panelField = new JPanel();
        panelControls = new JPanel();

        // ������� ������ ����� ����
        newB = new JButton("�����");
        
        scoreP1Label = new JLabel("����� 1: 0");
        scoreP2Label = new JLabel("����� 2: 0");
        statusLabel = new JLabel("����� ����������");

        // ���� ��� ���� �� ������
        buttons = new JButton[DataSet.WIDTH * DataSet.HEIGHT];
        for (int i = 0; i < DataSet.WIDTH * DataSet.HEIGHT; i++) 
        {
            buttons[i] = new JButton("");
            buttons[i].setOpaque(true);
            buttons[i].setBackground(Color.WHITE);
				
          
        }
        
        for(int x=0; x<DataSet.WIDTH; x++)
			for(int y=0; y<DataSet.HEIGHT; y++)
			{
				buttons[x + y*DataSet.HEIGHT].putClientProperty("x", x);
				buttons[x + y*DataSet.HEIGHT].putClientProperty("y", y);
			}
 
        
        //�������������� ����� ����������, �� �������� �� ������� ������
        action = new MyActionListener(this, data);

        // layout ���������� ��� ������ ��������� ����, ��������� ������
        frame.setLayout(new BorderLayout());
        frame.add(panelField, BorderLayout.CENTER);
        frame.add(panelControls, BorderLayout.SOUTH);

        // ������ - �����
        panelField.setLayout(new GridLayout(DataSet.HEIGHT, DataSet.WIDTH));
        for (int i = 0; i < DataSet.HEIGHT * DataSet.WIDTH; i++) 
        {
        	
        	//��������� ������ �� ������
        	panelField.add(buttons[i]);
            
            // ��������� �������� �� �������
            buttons[i].addActionListener(action);
        
            //���������� ������ ���������
            buttons[i].setEnabled(false);
        }

        // ������ ������ ����� ���� � ������
        panelControls.setLayout(new FlowLayout());     
        panelControls.add(newB);
        
        panelControls.add(scoreP1Label);
        panelControls.add(scoreP2Label);
        panelControls.add(statusLabel);

        // ������ ������ ����
        frame.setSize(580, 650);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // ��������� �������� ��� ������ ����� ����
        newB.addActionListener(action);
        newB.putClientProperty("action", "new_game");
    }
    
    /**
     * �������� ����������
     */
    public void declareResults()
    {
    	int p1 = myData.getPlayerScore(1);
    	int p2 = myData.getPlayerScore(2);
    	
    	if(p1>p2)
    		JOptionPane.showMessageDialog(frame, "����� 1 ������� �� ������: "+p1);
    	else if(p2>p1)
    		JOptionPane.showMessageDialog(frame, "����� 2 ������� �� ������: "+p2);
    	else
    		JOptionPane.showMessageDialog(frame, "�����!");
    }

    /**
     * �������� ��� ��������� ������
     * @param isEnable
     */
    public void enableButtons(boolean isEnable)
    {
    	for(JButton button : buttons)
    		button.setEnabled(isEnable);
    }
    
    /**
     * �������� ������� ����
     */
    public void clearButtons()
    {
    	scoreP1Label.setText("����� 1: 0");
        scoreP2Label.setText("����� 2: 0");
        
    	for(JButton button : buttons)
    	{
    		button.setEnabled(true);
    		button.setBackground(Color.WHITE);
    	}
    }
    
    /**
     * ������������ ������� ����
     */
    public void updateButtons()
    {
    
    	scoreP1Label.setText("����� 1: "+ myData.getPlayerScore(1));
        scoreP2Label.setText("����� 2: "+ myData.getPlayerScore(2));
    	
    	 for(int x=0; x<DataSet.WIDTH; x++)
 			for(int y=0; y<DataSet.HEIGHT; y++)
 			{
 				
 				
 				int[][] arr = myData.getFieldArray();
 				
 				if(arr[x][y] == 1)
 				{
 					buttons[x + y*DataSet.HEIGHT].setEnabled(false);
 					buttons[x + y*DataSet.HEIGHT].setBackground(Color.RED);
 				}else if(arr[x][y] == 2)
 				{
 					buttons[x + y*DataSet.HEIGHT].setEnabled(false);
 					buttons[x + y*DataSet.HEIGHT].setBackground(Color.ORANGE);
 				}else if(arr[x][y] == 0)
 				{
 					buttons[x + y*DataSet.HEIGHT].setEnabled(true);
 					buttons[x + y*DataSet.HEIGHT].setBackground(Color.WHITE);
 				}
 						
 			}
    	 
    }
    
   /**
    * ���������� � ������ �����
    * @param text ����� ��� �������
    */
    public void setStatus(String text)
    {
    	statusLabel.setText(text);
    }

    
}